<?php

/**
 * @file
 * Carousel Views style plugin.
 */

//----------------------------------------------------------------------------
// Drupal core hooks.

/**
 * Implementation of hook_perm().
 */
function views_carousel_perm() {
  return array('administer views_carousel');
}

/**
 * Implementation of hook_menu().
 */
function views_carousel_menu($may_cache) {
  $items = array();

  $items[] = array(
    'path' => 'admin/build/views/carousel',
    'title' => t('Carousels'),
    'callback' => 'drupal_get_form',
    'callback arguments' => array('views_carousel_settings_form'),
    'type' => MENU_LOCAL_TASK,
    'access' => user_access('administer views_carousel'),
  );
  $items[] = array(
    'path' => 'views_carousel_ahah',
    'callback' => 'views_carousel_ahah',
    'type' => MENU_CALLBACK,
    'access' => TRUE,
  );

  return $items;
}


//----------------------------------------------------------------------------
// Views hooks.

/**
 * Implementation of hook_views_style_plugins().
 */
function views_carousel_views_style_plugins() {
  $style_plugins['views_carousel'] = array(
    'name' => t('Carousel View'),
    'theme' => 'views_view_carousel',
    'validate' => 'views_ui_plugin_validate_list', // We want at least one field to be displayed, and so does the "List View" View style, so we use that validator.
    'needs_fields' => TRUE,
  );
  
  return $style_plugins;
}


//----------------------------------------------------------------------------
// Form callbacks.

/**
 * Form definition: module settings.
 */
 // @TODO: more granular settings!
function views_carousel_settings_form() {
  $form['global'] = array(
    '#type' => 'fieldset',
    '#title' => t('Global settings'),
    '#description' => t('If a carousel is not customly configured, then the global settings will be used.'),
  );
  
  return system_settings_form($form);
}


//----------------------------------------------------------------------------
// AHAH callbacks.

/**
 * Load carousel items.
 */
function views_carousel_ahah($view_name = NULL, $type = NULL) {
  $output = '';

  if (!$view_name) {
    exit;
  }
  
  if (!$type) {
    $type = 'block';
  }

  // Render the requested "page" of the view (the next set of items).
  $view = views_get_view($view_name);
  $args = explode('/', $_GET['args']);
  $filters = views_get_filter_values();
  $query = _views_get_query($view, $args, $filters);
  $total = db_num_rows(db_query($query['query'], $query['args']));
  // Do not render anything if the page count has gone over the limit. While
  // this shouldn't happen in theory, in practice it does, because the total
  // count of the items in a view might not be accurate. By default, Drupal
  // and pager_query() will continue to display the last 2 items even though
  // the $_GET['page'] arg has gone too high.
  $limit = $view->{'nodes_per_'. $type};
  if ($view->vid) {
    // The order of these if statements is important, otherwise we are dividing by zero
    if ($limit == 0) {
      $output = views_build_view($type, $view, $args, $view->use_pager, $view->{'nodes_per_'. $type});
    }
    elseif (($total / $limit) > $_GET['page']) {
      // We only set the pager when $limit is non-zero
      $view->use_pager = TRUE;
      $output = views_build_view($type, $view, $args, $view->use_pager, $view->{'nodes_per_'. $type});
    }
    else {
      // We don't have any content to return, so we just exit
    }
  }

  print $output;
  exit;
}


//----------------------------------------------------------------------------
// Themeing callbacks.

/**
 * Themable carousel.
 */
function theme_views_view_carousel($view, $nodes, $type) {
  static $js_added, $counter;

  $module_path = drupal_get_path('module', 'views_carousel');

  if (!$js_added) {
    jcarousel_add();
    drupal_add_js($module_path .'/views_carousel.js');
    drupal_add_js(array('viewsCarousel' => array('basePath' => base_path())), 'setting');

    $js_added = TRUE;
  }
  
  if (!$counter) {
    $counter = array();
  }
  if (!$counter[$view->vid]) {
    $counter[$view->vid] = 0;
  }
  $counter[$view->vid]++;
  $key = $view->vid .'-'. $counter[$view->vid];
  
  // Get the total number of items in this view.
  $view->use_pager = TRUE;
  $query = _views_get_query($view, $view->args, $view->filter);
  $total = db_num_rows(db_query($query['query'], $query['args']));

  // Save the settings for the carousel, these will be used by the JavaScript.
  $settings = array(
    'viewsCarousel' => array(
      'views' => array(
        $key => array(
          'selector' => '.views-view-carousel-'. $key,
          'view' => array(
            'name' => $view->name,
            'vid' => $view->vid,
            'args' => $view->args || array(),
            'limit' => $view->{'nodes_per_'. $type},
            'type' => $type,
          ),
          'settings' => array(
            'animation' => 750, // Animation speed.
            'auto' => 0,  // Autoscroll.
            'easing' => 'QuartEaseOut',
            'scroll' => 2, // How many to scroll.
            'size' => $total,
            'vertical' => FALSE, // Direction. (horizontal / vertical)
            'visible' => 2, // Visible items.
            'wrap' => NULL, // Wrap style.
          ),
        ),
      ),
    ),
  );
  drupal_add_js($settings, 'setting');


  // Add the CSS for the skin that will be used.
  // TODO: handle skins.
  $skin = 'default';
  drupal_add_css($module_path ."/skins/$skin/skin.css");


  // Render items returned by Views as a list.
  $items = array();
  $fields = _views_get_fields();  
  foreach ($nodes as $node) {
    $item = '';
    foreach ($view->field as $field) {
      if (!isset($fields[$field['id']]['visible']) && $fields[$field['id']]['visible'] !== FALSE) {
        if ($field['label']) {
          $item .= "<div class='view-label ". views_css_safe('view-label-'. $field['queryname']) ."'>";
          $item .= $field['label'];
          $item .= '</div>';
        }
        $item .= "<div class='view-field ". views_css_safe('view-data-'. $field['queryname']) ."'>";
        $item .= views_theme_field('views_handle_field', $field['queryname'], $fields, $field, $node, $view);
        $item .= '</div>';
      }
    }
    $items[] = "<div class='view-item ". views_css_safe('view-item-'. $view->name) ."'>$item</div>\n";
  }

  return theme('item_list', $items, NULL, 'ul', array('class' => 'jcarousel jcarousel-skin-'. $skin .' views-view-carousel-'. $key));
}
