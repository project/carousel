/**
 * @file
 * Setup views with jCarousel.
 */

/**
 * Terminology:
 * -item: a single item in the carousel, corresponds to a single item returned
 *        by a View (in Views 1, this is always a node)
 * -set: a set of items, the amount is the same as either the nodes_per_page
 *       or nodes_per_block properties of a View object, this should be at
 *       least 10 or more, to reduce the number of AHAH callbacks. (This
 *       means that it's roughtly the equivalent of a page in a paged View.)
 * -viewset: a set of items that is just as large as the number of visible
 *           items in the carousel
 */

/**
 * Performance optimization 1: smart loading of carousel items.
 *
 * To not put unnecessary load on the server, carousel items are only loaded
 * when they are needed, and not per viewset, but per set.
 */

/**
 * Performance optimization 2: smart caching of carousel items.
 *
 * At any point, the current viewset will be present in the HTML, and if
 * relevant the two next and/or previous viewsets will be present in the HTML
 * too. All other items that were loaded already, will be in the cache. The
 * advantage of this strategy is that this will prevent the choking of
 * less-capable browsers.
 */

/**
 * Important remark: jCarousel starts counting items at 1, NOT at zero!
 */

var viewsCarousel = { 
  carousel: {},
  loadPath: 'views_carousel_ahah',
  keepViewSets: 1
};

viewsCarousel.autoAttach = function() {
  var carousel_id;
  var selector;
  var settings;
  var carousel;
  var args;
  var vc;

  // Add our custom easing function.
  jQuery.easing['QuartEaseOut'] = function(p, t, b, c, d) {
    return -c * ((t=t/d-1)*t*t*t - 1) + b;
  };

  // Set up our carousels.
  for (carousel_id in Drupal.settings.viewsCarousel.views) {
    // Basic variables.
    viewsCarousel.carousel[carousel_id] = {};
    vc = viewsCarousel.carousel[carousel_id];
    vc.selector = Drupal.settings.viewsCarousel.views[carousel_id]['selector'];
    vc.settings = Drupal.settings.viewsCarousel.views[carousel_id]['settings'];
    vc.type = Drupal.settings.viewsCarousel.views[carousel_id]['view']['type'];

    // Variables used for smart loading and caching.
    vc.itemCache = {};
    vc.itemCount = Drupal.settings.viewsCarousel.views[carousel_id]['settings']['size'];
    vc.setSize = Drupal.settings.viewsCarousel.views[carousel_id]['view']['limit'];
    vc.setCount = Math.ceil(vc.itemCount / vc.setSize);
    vc.viewsetSize = vc.settings.visible;
    vc.viewsetCount = Math.ceil(vc.itemCount / vc.viewsetSize);
    vc.prevFirst = 1 - vc.viewsetSize;
    vc.nextFirst = 1 + vc.viewsetSize;
    vc.nextLast = 2 * vc.viewsetSize;
    vc.nextSet = 1;
    vc.count = vc.setSize;

    // Remove the pager.
    $('div.view-'+ carousel_id +' div.pager').remove();
    
    // Handle args for Views.
    args = Drupal.settings.viewsCarousel.views[carousel_id]['view']['args'];
    if (args.length > 0) {
      vc.args = args.join('/');
    }
    
    // jCarousel itemLoadCallback()
    vc.settings.itemLoadCallback = function(carousel, state) {
      if (state == 'next' || state == 'init') {   
        var currentCount = vc.nextSet * vc.setSize;
        var totalCount = vc.itemCount;

        // Load the next viewset if necessary.
        if ((vc.nextLast > currentCount && vc.nextFirst <= totalCount)
            ||
            (vc.nextLast + 1 > currentCount && vc.nextLast + 1 <= totalCount)
          )
        {
          viewsCarousel.loadNextSet(carousel_id, carousel);
        }
        if (state == 'next') {
          vc.nextFirst += vc.viewsetSize;
          vc.nextLast += vc.viewsetSize;
          vc.prevFirst += vc.viewsetSize;
        }
      }
      else if (state = 'back') {
        vc.nextFirst -= vc.viewsetSize; 
        vc.nextLast -= vc.viewsetSize;
        vc.prevFirst -= vc.viewsetSize;
      }
// @TODO: get this working
//      viewsCarousel.smartCache(carousel_id, carousel);
    };
    
    // Start jCarousel!
    $(vc.selector)
    .jcarousel(vc.settings);
  }
};

viewsCarousel.loadNextSet = function(carousel_id, carousel) {
  var vc = viewsCarousel.carousel[carousel_id];

  if (vc.nextSet < vc.setCount) {
    carousel.lock();

    var url = Drupal.settings.viewsCarousel.basePath + viewsCarousel.loadPath +'/'+ carousel_id +'/'+ vc.type;
    $.get(url, {page: vc.nextSet, args: vc.args || ''}, function(data) {
      // The total count of items in a view might not be accurate. When this
      // happens, we will ask for a page that doesn't exist in reality, and we
      // will get back nothing (an empty string). So check for that.
      if (data) {
        $('ul li', data).each(function() {
          vc.count++;
          carousel.add(vc.count, $(this).html());
        });
  
        vc.nextSet++;
      }
      else {
        // Adjust the incorrect total item count.
        vc.itemCount -= vc.setSize;
        carousel.size(carousel.size() - vc.setSize);
      }
    });

    carousel.unlock();
  }
};

// Move items outside of the previous/current/next viewsets to the cache.
viewsCarousel.smartCache = function(carousel_id, carousel) {
  if (viewsCarousel.keepViewSets > -1) {
    var vc = viewsCarousel.carousel[carousel_id];

    var i;
    var end;
    var viewsetId = (carousel.first - 1) / vc.viewsetSize + 1;
    var k = viewsCarousel.keepViewSets;

    carousel.lock();

    // Store the k-th previous viewset to the "back cache".
    toCache = viewsetId - k - 1;
    start = 1 + ((toCache - 1) * vc.viewsetSize);
    end = start - 1 + vc.viewsetSize;
    for (i = start; i >= 1 && i <= end && vc.itemCache[i] === undefined; i++) {
        vc.itemCache[i] = carousel.get(i).html();
        //carousel.size(carousel.size() - 1); // Doesn't help.
        carousel.remove(i);
        console.log("[back] cached: "+ i);
    }

    // Restore the (k+1)th previous viewset from the "back cache".
    start = 1 + (toCache * vc.viewsetSize);
    end = start - 1 + vc.viewsetSize;
    for (i = start; i >= 1 && i <= end && vc.itemCache[i] !== undefined; i++) {
      //carousel.size(carousel.size() + 1); // Doesn't help.
      carousel.add(i, $(vc.itemCache[i]));
      vc.itemCache[i] = undefined;
      console.log("[back] restored: "+ i);
    }
    carousel.reload();
    carousel.unlock();
  }
};


if (Drupal.jsEnabled) {
  $(document).ready(viewsCarousel.autoAttach);
}
